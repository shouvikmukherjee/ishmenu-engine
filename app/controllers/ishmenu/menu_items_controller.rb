require_dependency "ishmenu/application_controller"

module Ishmenu
  class MenuItemsController < ApplicationController
    before_action :set_menu_item, only: [:edit, :update, :destroy]
    before_action :authenticate_user!
    before_action :load_menu

    layout 'application'

    # GET /menu_items
    def index
      redirect_to menu_path(@menu)
    end


    # GET /menu_items/new
    def new
      @menu_item = MenuItem.new
    end

    # GET /menu_items/1/edit
    def edit
    end

    # POST /menu_items
    def create
      @menu_item = MenuItem.new(menu_item_params)

      if @menu_item.save
        redirect_to menu_path(@menu), notice: 'Menu item was successfully created.'
      else
        render :new
      end
    end

    # PATCH/PUT /menu_items/1
    def update
      if @menu_item.update(menu_item_params)
        redirect_to menu_path(@menu), notice: 'Menu item was successfully updated.'
      else
        render :edit
      end
    end

    # DELETE /menu_items/1
    def destroy
      @menu_item.destroy
      redirect_to :back, notice: 'Menu item was successfully destroyed.'
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_menu_item
        @menu_item = MenuItem.find(params[:id])
      end

      def load_menu
        @menu = Menu.friendly.find(params[:menu_id])
      end

      # Only allow a trusted parameter "white list" through.
      def menu_item_params
        params.require(:menu_item).permit(:menu_id, :title, :slug, :target, :item_id, :item_class, :priority)
      end
  end
end
