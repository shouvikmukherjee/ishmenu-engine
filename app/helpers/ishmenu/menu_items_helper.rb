module Ishmenu
  module MenuItemsHelper
  	def load_ishmenu_item(menu_item)
  		if !menu_item.target
  			content_tag :li do
	  			link_to menu_item.title, menu_item.slug,  :target => '_blank'
	  		end
  		else
  			content_tag :li do
	  			link_to menu_item.title, menu_item.slug, :target => '_self'
	  		end
  		end
  	end
  end
end
