module Ishmenu
  module ApplicationHelper
  	def load_ishmenu_item(menu_item)
  		if !menu_item.target
  			content_tag :li, :class => get_ishmenu_item_class(menu_item) do
	  			link_to menu_item.title, menu_item.slug,  :target => '_blank'
	  		end
  		else
  			content_tag :li, :class => get_ishmenu_item_class(menu_item) do
	  			link_to menu_item.title, menu_item.slug, :target => '_self'
	  		end
  		end
  	end

  	def get_ishmenu_item_class(menu_item)
  		if current_page?(menu_item.slug)
	  		'active'
  		end
  	end
  end
end
