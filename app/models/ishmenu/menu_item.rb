module Ishmenu
  class MenuItem < ActiveRecord::Base

  	# Association
  	belongs_to :menu

  	# Scope
  	scope :sorted, lambda { order(priority: :asc) }

  end
end
