module Ishmenu
  class Menu < ActiveRecord::Base

  		# This assumes you already have friendly_id in your gemfile and all that good stuff.
  		extend FriendlyId
  		friendly_id :slug, use: :slugged

  		# Association
  		has_many :menu_items, dependent: :destroy

  		# Fields validation
  		SLUG_REGEX = /\A^[\w&-]+$\Z/

  		validates :name, presence: true,
  		            length: { maximum: 250 }

  		validates :slug, presence: true,
			  		uniqueness: true,
  		            length: { maximum: 250 },
  		            format: SLUG_REGEX

  end
end
