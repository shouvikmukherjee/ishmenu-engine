# iShMenu Rails Engine
This is a simple ROR Engine for dynamically managing navigation menus on ROR apps.

## Dependencies
-	Devise: https://github.com/plataformatec/devise
-	Cancan: https://github.com/ryanb/cancan
-	Friendly Id: https://github.com/norman/friendly_id

## Installation
-	Install the gem
	```
	gem 'ishmenu', :git => 'git@bitbucket.org:shouvikmukherjee/ishmenu-engine.git'
	```

-	Copy Migrations
	```
	rake ishmenu:install:migrations
	```

-	Run Migrations
	```
	rake db:migrate
	```

-	Mount the Routes config
	```
	mount ishmenu::Engine => "/menu"
	```