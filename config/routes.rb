Ishmenu::Engine.routes.draw do
  

  resources :menus, :path => '/' do
  	resources :menu_items, :path => '/items', :as => 'items'
  end

end
