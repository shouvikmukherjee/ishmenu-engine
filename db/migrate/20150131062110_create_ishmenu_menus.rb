class CreateIshmenuMenus < ActiveRecord::Migration
  def change
    create_table :ishmenu_menus do |t|
      t.string :name
      t.string :slug

      t.timestamps null: false
    end
    add_index :ishmenu_menus, :slug
  end
end
