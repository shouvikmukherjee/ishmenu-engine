class CreateIshmenuMenuItems < ActiveRecord::Migration
  def change
    create_table :ishmenu_menu_items do |t|
      t.string :title
      t.string :slug
      t.boolean :target
      t.string :item_id
      t.string :item_class
      t.integer :priority
      t.belongs_to :menu, index: true

      t.timestamps null: false
    end
    add_index :ishmenu_menu_items, :slug
  end
end
