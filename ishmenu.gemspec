$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "ishmenu/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "ishmenu"
  s.version     = Ishmenu::VERSION
  s.authors     = ["Shouvik Mukherjee"]
  s.email       = ["contact@ishouvik.com"]
  s.homepage    = "http://ishouvik.com"
  s.summary     = "Create and manage menu items dynamically."
  s.description = "Create and manage menu items dynamically."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.0"
  s.add_dependency "cancan"
  s.add_dependency "friendly_id"
  s.add_dependency "sass-rails"

  s.add_development_dependency "sqlite3"
end
