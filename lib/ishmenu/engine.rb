module Ishmenu
  class Engine < ::Rails::Engine
    isolate_namespace Ishmenu
  end

  # config.to_prepare do
  #   ApplicationController.helper(AnnouncementsHelper)
  # end
  
  require 'cancan'
  require 'friendly_id'
end
