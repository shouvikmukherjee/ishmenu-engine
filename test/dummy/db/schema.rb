# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150131062938) do

  create_table "ishmenu_menu_items", force: :cascade do |t|
    t.string   "title"
    t.string   "slug"
    t.boolean  "target"
    t.string   "item_id"
    t.string   "item_class"
    t.integer  "priority"
    t.integer  "menu_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "ishmenu_menu_items", ["menu_id"], name: "index_ishmenu_menu_items_on_menu_id"
  add_index "ishmenu_menu_items", ["slug"], name: "index_ishmenu_menu_items_on_slug"

  create_table "ishmenu_menus", force: :cascade do |t|
    t.string   "name"
    t.string   "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "ishmenu_menus", ["slug"], name: "index_ishmenu_menus_on_slug"

end
